package com.zola.invoices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.zola.invoices.dao.InvoiceDao;
import com.zola.invoices.model.Invoice;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceDao invoiceDao;

    public Invoice saveInvoice(Invoice invoice) {

        return invoiceDao.save(invoice);
    }

    public List<Invoice> getInvoices(String invoiceNumber, String purchaseOrderNumber, Pageable pageable) {

        return invoiceDao.findInvoicesByInvoiceNumberOrPurchaseOrderNumber(invoiceNumber, purchaseOrderNumber, pageable);
    }

}
