package com.zola.invoices.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.zola.invoices.model.Invoice;

@Repository
public interface InvoiceDao extends JpaRepository<Invoice, Long> {

    @Query(value = "SELECT i FROM Invoice i WHERE i.invoiceNumber = ?1 OR i.poNumber = ?2 ORDER BY i.createdAt DESC")
    List<Invoice> findInvoicesByInvoiceNumberOrPurchaseOrderNumber(String invoiceNumber, String purchaseOrderNumber, Pageable pageable);

}
