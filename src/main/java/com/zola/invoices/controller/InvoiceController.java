package com.zola.invoices.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.zola.invoices.model.Invoice;
import com.zola.invoices.service.InvoiceService;

@Controller
@RequestMapping("/v1/invoices")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @PutMapping
    public ResponseEntity<Invoice> saveInvoice(@RequestBody @Valid Invoice invoice) {
        return new ResponseEntity<>(invoiceService.saveInvoice(invoice), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Invoice>> getInvoicesByInvoiceNumberOrPoNumber(@RequestParam(value = "invoice_number") String invoiceNumber, @RequestParam(value = "po_number") String poNumber, @PageableDefault(size = 50) Pageable pageable) {
        return new ResponseEntity<>(invoiceService.getInvoices(invoiceNumber, poNumber, pageable), HttpStatus.OK);
    }

}
