package com.zola.invoices.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zola.invoices.dao.InvoiceDao;
import com.zola.invoices.model.Invoice;

@RunWith(SpringJUnit4ClassRunner.class)
public class InvoiceServiceTest {

    @Mock
    private InvoiceDao invoiceDao;

    @InjectMocks
    private InvoiceService classUnderTest;

    @Test
    public void saveInvoiceTest() {
        Invoice invoice = Mockito.mock(Invoice.class);
        classUnderTest.saveInvoice(invoice);
        Mockito.verify(invoiceDao, Mockito.times(1)).save(invoice);
    }

    @Test
    public void getInvoicesTest() {
        classUnderTest.getInvoices(anyString(), anyString(), any(Pageable.class));
        Mockito.verify(invoiceDao, Mockito.times(1)).findInvoicesByInvoiceNumberOrPurchaseOrderNumber(anyString(), anyString(), any());
    }
}
