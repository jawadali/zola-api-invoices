package com.zola.invoices.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zola.invoices.model.Invoice;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class InvoiceControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testControllerSave() throws Exception {
        LocalDate dueDate = LocalDate.parse("2019-06-16");
        Invoice invoice = Invoice.builder()
                            .invoiceNumber("1")
                            .poNumber("2")
                            .dueDate(dueDate)
                            .amountCents(1000L)
                            .build();
        MockHttpServletResponse result = mockMvc.perform(put("/v1/invoices")
                                                .contentType("application/json")
                                                .accept("application/json")
                                                .content(objectMapper.writeValueAsString(invoice)))
                                                .andReturn()
                                                .getResponse();
        Assert.assertEquals(HttpStatus.CREATED.value(), result.getStatus());
        invoice = objectMapper.readValue(result.getContentAsString(), Invoice.class);
        Assert.assertNotNull(invoice);
        Assert.assertEquals("1", invoice.getInvoiceNumber());
        Assert.assertEquals("2", invoice.getPoNumber());
        Assert.assertEquals(Long.valueOf(1000), invoice.getAmountCents());
        Assert.assertNotNull(invoice.getCreatedAt());
        Assert.assertEquals(dueDate, invoice.getDueDate());
    }

}
