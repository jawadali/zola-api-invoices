# Zola Api Invoices

Zola Api Invoices is a spring boot web application for saving and retrieving invoices.

## Source

Clone the project from git clone ```https://jawadali@bitbucket.org/jawadali/zola-api-invoices.git```


## Running the App

It is a spring-boot Java project, simply run it in IntelliJ IDE as a spring-boot application or run ```mvn spring-boot:run``` in a shell.

## Frameworks
This app uses spring boot (Java 8) along with Java Persistence API and an in-memory H2 database.

JUnit is used for unit testing alongside with Jacoco with branch coverage set to 90% in order to ensure a good testing coverage.

PMD is used as a static code analyzer and checkstyle is used to ensure a proper coding standard (Note: The checkstyles.xml used is from Google's checkstyle file with a little modification).

## Limitations
This project does not include client code to interact with the rest API(s) of this web application.

## Assumptions
The description mentioned to use 'YYYY-MM-DD' for the due date field. In Java, `Y` translates to 'Week Year' while `y` translates to regular calendar year. From the sample request and response json, I assumed the purpose was to use ISO date format (i.e. 2019-06-16) so I took the liberty of using `@DateTimeFormat(iso = ISO.DATE)`.